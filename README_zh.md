---
license: other
---


![Aquila_logo](./log.jpeg)


<h4 align="center">
    <p>
        <a href="https://huggingface.co/BAAI/Aquila2-70B-Expr/blob/main/README.md">English</a> 
        <b>简体中文</b> |
    </p>
</h4>

# 悟道·天鹰（Aquila2）

我们开源了我们的 **Aquila2** 系列，现在包括基础语言模型 **Aquila2-7B**，**Aquila2-34B** 和 **Aquila2-70B-Expr** ，对话模型 **AquilaChat2-7B**，**AquilaChat2-34B** 和**AquilaChat2-70B-Expr** ，长文本对话模型**AquilaChat2-7B-16k** 和 **AquilaChat2-34B-16k**

悟道 · 天鹰 Aquila 模型的更多细节将在官方技术报告中呈现。请关注官方渠道更新。

## 快速开始使用

## 使用方式/How to use

### 1. 推理/Inference

```python
import torch
from transformers import AutoTokenizer, AutoModelForCausalLM
from transformers import BitsAndBytesConfig

model_info = "BAAI/Aquila2-70B-Expr"
tokenizer = AutoTokenizer.from_pretrained(model_info, trust_remote_code=True)
model = AutoModelForCausalLM.from_pretrained(model_info, trust_remote_code=True)
model.eval()
text = "请给出10个要到北京旅游的理由。"
tokens = tokenizer.encode_plus(text)['input_ids']
tokens = torch.tensor(tokens)[None,].to(device)
stop_tokens = ["###", "[UNK]", "</s>"]
with torch.no_grad():
    out = model.generate(tokens, do_sample=True, max_length=512, eos_token_id=100007, bad_words_ids=[[tokenizer.encode(token)[0] for token in stop_tokens]])[0]
    out = tokenizer.decode(out.cpu().numpy().tolist())
    print(out)
```


## 证书/License

Aquila2系列开源模型使用 [智源Aquila系列70B模型许可协议](https://huggingface.co/BAAI/Aquila2-70B-Expr/blob/main/BAAI-Aquila-70B-Model-License-Agreement.pdf)